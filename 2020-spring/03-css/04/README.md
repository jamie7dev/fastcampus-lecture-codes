# 04. CSS 기본 스타일 2

## 상자 관련 스타일

<br>

### `width`, `height` : 높이와 너비
| 단위 | 설명 | 
| :-- | :-- |
| `px` | 절대 크기: 모니터상의 한 점 |
| `%` | 상대 크기: 바로 윗 부모의 크기에 비례 |
| `vw` | 상대 크기: 뷰포트 너비의 1/100 |
| `vh` | 상대 크기: 뷰포트 높이의 1/100 |
| `vmax` | 상대 크기: 뷰포트 너비, 높이 중 긴 쪽의 1/100 |
| `vmin` | 상대 크기: 뷰포트 너비, 높이 중 짧은 쪽의 1/100 |

```html
  <div class="outer">
    <div class="inner"></div>
  </div>
```
```css
/* 기본 세팅 */
body {
  padding: 0;
  margin: 0;
}
.outer { 
  width: 80%;
  height: 640px;
  background-color: yellowgreen;
}
.inner { background-color: green; }
```  

<br>

> 아래부터
```css
.inner {
  width: 400px;
  height: 300px;
}
```
```css
.inner {
  width: 50%;
  height: 50%;
}
```
```css
.inner {
  width: 50vw;
  height: 50vh;
}
```
```css
.inner {
  width: 50vmin;
  height: 50vmin;
}
```
```css
.inner {
  width: calc(100% - 100px);
  height: calc(50vh + 200px);
}
```

<br>
<br>

### `margin` : 바깥쪽 여백

```html
  <div class="outer">
    <div class="inner">inner div</div>
    <div class="inner">inner div</div>
    <div class="inner">inner div</div>
  </div>
```
```css
/* 기본 세팅 */
body {
  padding: 0;
  margin: 0;
}
.outer { 
  background-color: yellowgreen;
}
.inner { 
  color: white;
  background-color: green; 
}
```

<br>

> 아래부터
```css
.inner {
  margin: 24px;
}
```
```css
.inner {
  margin-top: 24px;
  margin-right: 48px;
  margin-bottom: 0;
  margin-left: 12px;
}
```
```css
.inner {
  margin: 24px 48px 0 12px;
}
```
```css
.inner {
  margin: 12px 48px;
}
```
```css
.inner {
  width: 300px;
  margin: 12px auto;
}
```

<br>
<br>

### `padding` : 안쪽 여백
```css
.outer {
  padding: 24px;
}
```
```css
.outer {
  padding-top: 24px;
  padding-right: 48px;
  padding-bottom: 0;
  padding-left: 12px;
}
```
```css
.outer {
  padding: 24px 48px 0 12px;
}
```
```css
.outer {
  padding: 12px 48px;
}
```

<br>
<br>

### `border` : 테두리 선
border: (`선 굵기`) (`선 스타일`) (`선 색`);

<br>

| 선 스타일 | 설명 |  
| :-- | :-- |
| `solid` | 직선 |
| `dashed` | 점이 긴 점선 |
| `dotted` | 점선 |
> * <a href="https://developer.mozilla.org/ko/docs/Web/CSS/border-style" target="_blank">더 자세히 보기 (MDN)</a>

<br>

```html
  <div>테두리가 있는 div</div>
```
```css
/* 기본 세팅 */
div {
  margin: 24px;
  padding: 24px;
}
```  

<br>

> 아래부터
```css
div {
  border: 1px solid black;
}
```
```css
div {
  border-top: 1px solid black;
  border-right: 3px dashed green;
  border-bottom: 9px dotted #ff6347;
  border-left: 0;
}
```

<br>
<br>

### `border-radius` : 둥근 모서리

```html
  <div>둥근 모서리</div>
```
```css
/* 기본 세팅 */
div {
  width: 400px;
  height: 400px;
  line-height: 400px;
  text-align: center;
  color: white;
  background-color: orange;
  border: 2px solid darkorange;
}
```

<br>

> 아래부터

```css
div {
  border-radius: 8px;
}
```
```css
div {
  border-radius: 50%;
}
```
```css
div {
  border-radius: 24px 0 25% 50%;
}
```

<br>
<br>

### `background` : 배경  

| 속성 | 설명 |  
| :-- | :-- |
| `background-color` | 배경색 |
| `background-image` | 배경 이미지 |
| `background-size` | 배경 이미지 크기 |
| `background-position` | 배경 이미지 위치 |
| `background-repeat` | 배경 이미지 반복 여부 |

```html
  <div></div>
```
```css
/* 기본 세팅 */
div {
  width: 480px;
  height: 360px;
  border: 1px solid black;
}
```
<br>

> 아래부터
```css
div { 
  background-color: green; 
}
```
```css
div { 
  background-color: rgba(255, 255, 0, 0.5); 
}
```
```css
div {
  background-image: url("./nature.jpg");
}
```
```css
div {
  background-image: url("./nature.jpg");
  background-size: 480px 360px;
}
```
```css
div {
  background-image: url("./nature.jpg");
  background-size: 50% 50%;
  background-repeat: no-repeat;
  background-position: 50px 50%;
}
```
```css
div {
  /* 비율 유지, 상자에 빈 곳이 없도록 꽉 채움 */
  background-image: url("./nature.jpg");
  background-size: cover;
}
```
```css
div {
  /* 비율 유지, 상자를 벗어나지 않도록 꽉 채움 */
  background-image: url("./nature.jpg");
  background-size: contain;
}
```

<br>
<br>

***

<br>
<br>

## `box-shadow` : 그림자
box-shadow: (`x축 위치`) (`y축 위치`) ( [옵션] `번짐`) (`색상`)

```html
  <div></div>
```
```css
/* 기본 세팅 */
body { background-color: lightgray; }
div {
  background-color: white;
  width: 320px;
  height: 120px;
  border-radius: 16px;
}
```
<br>

> 아래부터
```css
div {
  box-shadow: 1px 1px black;
}
```
```css
div {
  box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.25);
}
```
```css
div {
  box-shadow: 
  0 0 4px red,
  2px 2px 8px orange,
  4px 2px 12px yellow;
}
```

<br>
<br>

***

<br>
<br>

## 다음 강좌
* [05. CSS 레이아웃](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/05/README.md)