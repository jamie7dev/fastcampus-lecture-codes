# 01. Firebase 호스팅

<a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/04-javascript/final-practice/final-practice.zip" target="_blank">
  [실습파일 다운로드 받기]
</a>

## 웹 호스팅
웹사이트를 배포할 수 있도록 인터넷망에 연결된 서버의 일정 공간을 임대해주는 서비스
* `서버` : *(역할의 개념)* 제공자 역할을 하는 컴퓨터
* `호스팅` : 서버 임대 서비스
  * 웹 호스팅, 메일 호스팅, 쇼핑몰 호스팅, 서버 호스팅...
  * <a href="https://namu.wiki/w/%ED%98%B8%EC%8A%A4%ED%8C%85/%EC%A3%BC%EC%9A%94%20%ED%98%B8%EC%8A%A4%ED%8C%85%EC%82%AC%20%EB%AA%A9%EB%A1%9D" target="_blank">업체 목록 보기</a>

* `.html`, `.css`, `.js`, 이미지 등 정적 파일들을 업로드하여 외부에서 접근 가능하도록 함 

<br>

***

<br>

## Firebase

서버에 직접 구현해야 했던 각종 기능들을 대신해주는 서비스
* 호스팅, 데이터베이스, 파일저장, 로그인, 분석 등

<br>

### I. 파이어베이스 둘러보기 
<a href="https://firebase.google.com/" target="_blank">https://firebase.google.com/</a>
> 구글에서 `firebase` 검색 

<br>

### II. 파이어베이스 사전준비
1. `Node.js` 설치하기  
  > `javascript`를 컴퓨터에서 직접 돌릴 수 있도록 해 주는 환경
2. `console`에서 확인  
```
  npm -v
```

<br>

### III. 프로젝트 추가하기

1. 구글 로그인 *(`gmail` 계정)*  
2. 파이어베이스 페이지에서 `콘솔로 이동`  
    * `firebase console` 검색
3. `프로젝트 추가`

<br>

### IV. 프로젝트에 웹 앱 추가하기

1. `웹`앱 추가
> Firebase 호스팅은 여기서는 설정하지 않음
2. 스크립트 복사하여 추가

<br>

### V. `Hosting` 사용해보기
정적 웹을 온라인에 배포할 수 있도록 해주는 서비스

1. 콘솔에서 `Hosting` 메뉴로 이동  
2. `시작하기`를 눌러 배포과정 진행
3. 배포된 사이트 확인하기

<br>

***

<br>

## 다음 강좌  
* [02. Firebase 데이터베이스](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/05-firebase/02/README.md)