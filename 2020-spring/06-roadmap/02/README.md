# 프론트엔드 개발자 로드맵

## 📚 Command line interface
> `Powershell`(Win), `Termianl`(Mac)  
> *  **G**raphic **U**ser **I**nterface와 상대적 개념(?)

![cli](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/02/images/01.png)   

* 디렉토리 관련 명령어 `(이동, 생성, 삭제...)`
* git 사용법

<br>

***

<br>

## 📚 <a href="https://developer.mozilla.org/ko/docs/Web/Guide/AJAX/Getting_Started" target="_blank">Ajax 개념</a>
`비동기`식 자바스크립트

```javascript
var httpRequest;

function makeRequest() {
  httpRequest = new XMLHttpRequest();

  httpRequest.onreadystatechange = showContents;
  httpRequest.open('GET', 'test.html');
  httpRequest.send();
}

function showContents() {
  if (httpRequest.readyState === XMLHttpRequest.DONE) {
    if (httpRequest.status === 200) {
      console.log("받아온 정보: " + httpRequest.responseText);
    } else {
      console.log('정보를 받아오는데 실패했습니다.');
    }
  }
}
```

<br>

***

<br>

## 📚 <a href="https://nodejs.org/ko/" target="_blank">Node.js</a>
### Javascript runtime - 프론트엔드 **`필수요소`**
> 브라우저 밖에서 동작하는 Javascript  

![nodejs](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/02/images/02.png)   

* Javascript ES6 & ES7
```javascript
    const x = 1;
    let y = "Hello World"
    let z = () => { console.log(y) }
```
* Package manager
  - <a href="https://www.npmjs.com/" target="_blank">NPM</a>
  - <a href="https://classic.yarnpkg.com/en/" target="_blank">Yarn</a>
* Module bundler
  - <a href="https://webpack.js.org/" target="_blank">Webpack</a>

### *`다음 내용들을 진행하면서 배워갈 것`*

<br>

***

<br>

## 📚 SPA Framework 

![spa](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/02/images/03.png)   

* ### <a href="https://reactjs.org/" target="_blank">React</a> : 페이스북에서 제작, 가장 널리 쓰이는 프레임워크
* ### <a href="https://vuejs.org/" target="_blank">Vue</a> : 빠르게 성장중인 프레임워크
* ### <a href="https://angular.io/" target="_blank">Angular</a> : 구글에서 개발, 대규모 프로젝트에 사용

### Vue 예시

> 데이터
```javascript
var items = [
  { idx: 0, name: '홍길동' },
  { idx: 1, name: '전우치' },
  { idx: 2, name: '임꺽정' },
  { idx: 3, name: '연흥부' },
  { idx: 4, name: '김선달' }
]
```

> 목적
```html
<ul>
  <li>홍길동</li>
  <li>전우치</li>
  <li>임꺽정</li>
  <li>연흥부</li>
  <li>김선달</li>
</ul>
```

Vue로 작성한 코드
```html
<ul>
  <li v-for="item in items" v-bind:key="item.idx">{{ item.name }}</li>
</ul>
```

<br>

***

<br>

## 📚 CSS preprocessor
![sass](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/02/images/04.png)   

### `CSS`를 보다 효율적으로 작성
> <a href="https://sass-lang.com/" target="_blank">Sass</a>, <a href="https://postcss.org/" target="_blank">PostCSS</a>, <a href="http://lesscss.org/" target="_blank">less</a>

### 🔍 <a href="https://sass-lang.com/guide" target="_blank">Sass 예시 </a>


<br>

***

<br>

## 📚 <a href="https://www.typescriptlang.org/" target="_blank">Typescript</a>
![typescript](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/02/images/05.png) 

* 자바스크립트로 `컴파일`되는 언어
* 보다 엄격한 문법 - 오류의 소지를 줄여줌

<br>

> JavaScript
```javascript
function firstNChars (str, num) {
  return str.substr(0, Math.min(str.length, num));
}

let x = firstNChars('abcdefghijklmn', 5);
let y = firstNChars(10, 5); // 실행시 오류
```

<br>

> TypeScript
```typescript
function firstNChars (str: string, num: number): string {
  return str.substr(0, Math.min(str.length, num));
}

let y = firstNChars(10, 5); // 컴파일시 오류
```

<br>

***

<br>

## 다음 강좌
* [03. 백엔드 개발자](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/06-roadmap/03/README.md)